# gitlab-create-local-copy

#### Example usage

By default, auth reading values from env, cli will override this values. If all values are empty - will be used anonymous account.

##### via token

    export GITLAB_URL="https://gitlab.com"
    export GITLAB_TOKEN="afjhjkaDHjka"
    
    gitlab-create-local-gitlab-copy ~/Projects/gitlab.com

##### via token

    export GITLAB_URL="https://gitlab.com"
    export GITLAB_EMAIL="git@gitlab.com"
    export GITLAB_PASSWORD="afjhjkaDHjka"
    
    gitlab-create-local-gitlab-copy ~/Projects/gitlab.com

#### Help:

    usage: app.py [-h] [--url URL] [--private_token PRIVATE_TOKEN]
                  [--oauth_token OAUTH_TOKEN] [--email EMAIL]
                  [--password PASSWORD]
                  basedir
    
    Dump your gitlab
    
    positional arguments:
      basedir               base directory for download
    
    optional arguments:
      -h, --help            show this help message and exit
      --url URL             $GITLAB_URL=""
      --private_token PRIVATE_TOKEN
                            $GITLAB_TOKEN=""
      --oauth_token OAUTH_TOKEN
                            $GITLAB_OAUTH=""
      --email EMAIL         $GITLAB_EMAIL=""
      --password PASSWORD   $GITLAB_PASSWORD=""


