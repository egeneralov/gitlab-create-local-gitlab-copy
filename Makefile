gitlab-create-local-gitlab-copy:
	pyinstaller --distpath . --console --onefile --name gitlab-create-local-gitlab-copy app.py

clean:
	find . -name __pycache__ -type d | xargs rm -rf app.spec build
	rm -rf app.spec build app
