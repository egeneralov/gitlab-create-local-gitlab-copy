FROM python:3

WORKDIR /app/
RUN pip install pyinstaller
ADD requirements.txt /app/
RUN pip install --no-cache-dir -r /app/requirements.txt
ADD . .
RUN pyinstaller --distpath . --console --onefile --name gitlab-create-local-gitlab-copy app.py


FROM debian:10
RUN apt-get update -q && apt-get install -yq git && apt-get clean -yq
COPY --from=0 /app/gitlab-create-local-gitlab-copy /
ENTRYPOINT ["/gitlab-create-local-gitlab-copy"]
