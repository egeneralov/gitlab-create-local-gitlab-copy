#!/usr/bin/env python3

import os
import sys
import json
import argparse

import logging
import json_logging

import git
import gitlab
import gitlab.v4
import gitlab.v4.objects


json_logging.ENABLE_JSON_LOGGING = True
json_logging.init()
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(
  logging.StreamHandler(
    sys.stdout
  )
)


def auth(**kwargs):
  options = {}
  # update with parsed args
  for key in kwargs:
    options.update(**kwargs)
  # remove empty keys
  for key in options.copy().keys():
    if not options[key]:
      options.pop(key)
  gl = gitlab.Gitlab(**options)
  if not gl.auth():
    return gl
  raise Exception("Auth failed")


def parseargs():
  parser = argparse.ArgumentParser(
    description = 'Dump your gitlab'
  )
  
  parser.add_argument(
    'basedir',
    help = f'base directory for download'
  )
  
  url = os.environ.get('GITLAB_URL', 'https://gitlab.com')
  parser.add_argument(
    '--url',
    default = url,
    help = f'$GITLAB_URL="{url}"'
  )
  
  private_token = os.environ.get('GITLAB_TOKEN', '')
  parser.add_argument(
    '--private_token',
    default = private_token,
    help = f'$GITLAB_TOKEN="{private_token}"'
  )
  
  oauth_token = os.environ.get('GITLAB_OAUTH', '')
  parser.add_argument(
    '--oauth_token',
    default = oauth_token,
    help = f'$GITLAB_OAUTH="{oauth_token}"'
  )
  
  email = os.environ.get('GITLAB_EMAIL', '')
  parser.add_argument(
    '--email',
    default = email,
    help = f'$GITLAB_EMAIL="{email}"'
  )
  
  password = os.environ.get('GITLAB_PASSWORD', '')
  parser.add_argument(
    '--password',
    default = password,
    help = f'$GITLAB_PASSWORD="{password}"'
  )
  
  return parser.parse_args()


def main():
  args = parseargs()
  
  gl = auth(
    url = args.url,
    private_token = args.private_token,
    oauth_token = args.oauth_token,
    email = args.email,
    password = args.password
  )
  
  for group in gl.groups.list(as_list=False):
    for project in group.projects.list(as_list=False):
      logger.info(f"git clone {project.ssh_url_to_repo} {args.basedir}/{project.path_with_namespace}")
      directory = f"{args.basedir}/{project.path_with_namespace}"
      #.replace(project.name, '')
      if os.path.isdir(directory):
        logging.warning(f"{directory} already exist, skip")
        continue
      path = '/'
      dir = directory.split('/')
      dir.remove('')
      for i in dir:
        path += i + '/'
        try:
          os.mkdir(path)
        except FileExistsError:
          pass
#       git.Git(directory).clone(f"{project.ssh_url_to_repo}")
      repo = git.Repo.init(
        os.path.join(f"{args.basedir}/{project.path_with_namespace}")
      )
      origin = repo.create_remote('origin', f"{project.ssh_url_to_repo}")
      origin.fetch()
      try:
        logger.info('create local branch master from remote master')
        repo.create_head('master', origin.refs.master)
      except Exception as err:
        logger.error(f'failed: {err}')
      try:
        logger.info('create local branch master from remote master')
        repo.heads.master.set_tracking_branch(origin.refs.master)  # set local master to track remote master
      except Exception as err:
        logger.error(f'failed: {err}')
      try:
        logger.info('checkout local master to working tree')
        repo.heads.master.checkout()
      except Exception as err:
        logger.error(f'failed: {err}')
        try:
          logger.info('create head master')
          origin.create_head('master', origin.refs.master).set_tracking_branch(origin.refs.master).checkout()
        except Exception as err:
          logger.error(f'failed: {err}')

      try:
        logger.info('git pull')
        origin.pull()
      except Exception as err:
        logger.error(f'failed: {err}')




if __name__ == '__main__':
  try:
    main()
  except KeyboardInterrupt:
    sys.exit(0)
  except Exception as err:
    logger.critical(
      f"general failure: {err}"
    )
    sys.exit(1)


